$(document).ready(function() {

    $("form").submit( function(eventObj) {

        if($('#select_tags').length) {

            var data = $('#select_tags').select2('data');
            var collection = '';

            for (var i = 0; i < data.length; i++) {
                collection += data[i]['id']/* + ':' + data[i]['text']*/;
                if (i < data.length - 1) {
                    collection += ';';
                }
            }

            $("#tag_collection").val(collection);
            return true;
        }
    });


    $('#select_tags').select2({
        tags: false,
        multiple: true,
        maximumSelectionSize: 10,
        minimumInputLength: 1,
        width: $(this).parents('.form-group:first').width(),
        ajax: {
            url: get_tags_route,
            dataType: "json",
            type: "POST",
            data: function (params) {
                return {
                    "_token": csrf_token,
                    q: params.term,
                    page: params.page
                };
            },
            processResults: function (data) {
                var results = [];
                $.each(data.data, function (index, tag) {
                    results.push({
                        id: tag.tag_id,
                        text: tag.name
                    });
                });

                //console.log($('#select_tags').select2('data'));

                console.log(results);

                return {
                    results: results
                };
            },
            cache: true
        }
    });

});
