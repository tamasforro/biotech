<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use \Astrotomic\Translatable\Translatable;

    protected $table = 'products';
    protected $dates = ['created_at', 'updated_at'];

    public $translatedAttributes = ['name', 'description', 'price'];

    protected $fillable = [
        'status',
        'publish_start',
        'publish_end'
    ];

    public function getStatus(){
        return trans(StaticTypes::$status_types[$this->status]);
    }

    public function getStatusForTable(){
        return '<span class="badge badge-' . StaticTypes::$status_types_badges[$this->status] . '">' . trans(StaticTypes::$status_types[$this->status]) . '</span>';
    }
}
