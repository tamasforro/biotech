<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTags extends Model
{
    protected $table = 'product_tags';
    protected $fillable = [
        'status', 'product_id', 'tag_id'
    ];

    public function getStatus(){
        return trans(StaticTypes::$status_types[$this->status]);
    }

    public function getStatusForTable(){
        return '<span class="badge badge-' . StaticTypes::$status_types_badges[$this->status] . '">' . trans(StaticTypes::$status_types[$this->status]) . '</span>';
    }

    public function tag(){
        return $this->hasOne('\App\Tag', 'id', 'tag_id');
    }

    public function product(){
        return $this->hasOne('\App\Product', 'id', 'product_id');
    }

    public static function createTag($tag_id, $product_id){
        $exists = ProductTags::where('tag_id', $tag_id)->where('product_id', $product_id)->first();

        if(!$exists){
            $product_tag = new ProductTags();
            $product_tag->tag_id = $tag_id;
            $product_tag->product_id = $product_id;
            $product_tag->status = 1;
            if($product_tag->save()){
                return $result = [
                    'status' => 'success',
                    'message' => 'Product tag creation success!'
                ];
            }
            else{
                return $result = [
                    'status' => 'error',
                    'message' => 'Error while saving product tag!'
                ];
            }
        }
        else{
            if($exists->status == 0){
                $exists->status = 1;
                $exists->save();
            }

            return $result = [
                'status' => 'success',
                'message' => 'Product tag creation success!'
            ];
        }
    }
}
