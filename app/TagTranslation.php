<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagTranslation extends Model
{
    public $timestamps = false;
    protected $table = 'tag_translations';
    protected $fillable = [
        'name'
    ];
}
