<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductDocuments extends Model
{
    protected $table = 'product_documents';
    protected $fillable = [
        'status', 'product_id', 'path'
    ];

    public static $storage_path = 'products/documents/';
    public static $document_path = 'app/public/products/documents/';

    public function getStatus(){
        return trans(StaticTypes::$status_types[$this->status]);
    }

    public function getStatusForTable(){
        return '<span class="badge badge-' . StaticTypes::$status_types_badges[$this->status] . '">' . trans(StaticTypes::$status_types[$this->status]) . '</span>';
    }

    public function product(){
        return $this->hasOne('\App\Product', 'id', 'product_id');
    }

    public static function createDocument($file, $product_id){
        $filename = Str::random(40) . '.' . $file->getClientOriginalExtension();


        if(!File::exists(storage_path('app/public/'. ProductDocuments::$storage_path . '/' . $product_id . '/'))) {
            File::makeDirectory(storage_path('app/public/'. ProductDocuments::$storage_path . '/' . $product_id . '/'), 0775, true);
        }

        if($file->storeAs(ProductDocuments::$storage_path . '/' . $product_id, $filename, 'public')){
            $document = new ProductDocuments();
            $document->product_id = $product_id;
            $document->path = $filename;
            $document->status = 1;

            if($document->save()){
                return $result = [
                    'status' => 'success',
                    'message' => 'Conference document creation success!'
                ];
            }
            else{
                return $result = [
                    'status' => 'error',
                    'message' => 'Error while saving conference document!'
                ];
            }
        }
        else{
            return $result = [
                'status' => 'error',
                'message' => 'Error while uploading conference document!'
            ];
        }
    }

    public function getDocumentPath() {
        //return Storage::disk('public')->get(ConferenceDocuments::$storage_path . $this->conference_id . '/' . $this->path);
        return asset('storage/' . ProductDocuments::$storage_path . $this->product_id . '/' . $this->path);

    }

    public function getDocument() {
        return Storage::disk('public')->get(ProductDocuments::$storage_path . $this->conference_id . '/' . $this->path);

        //return asset( 'storage/' . ConferenceDocuments::$document_path . $this->conference_id . '/' . $this->path);
    }
}
