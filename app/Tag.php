<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use \Astrotomic\Translatable\Translatable;

    protected $table = 'tags';
    protected $dates = ['created_at', 'updated_at'];



    public $translatedAttributes = ['name'];

    protected $fillable = [
        'status'
    ];

    public function getStatus(){
        return trans(StaticTypes::$status_types[$this->status]);
    }

    public function getStatusForTable(){
        return '<span class="badge badge-' . StaticTypes::$status_types_badges[$this->status] . '">' . trans(StaticTypes::$status_types[$this->status]) . '</span>';
    }

}
