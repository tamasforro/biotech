<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    public $timestamps = false;
    protected $table = 'product_translations';
    protected $fillable = [
        'name',
        'description',
        'price'
    ];
}
