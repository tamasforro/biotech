<?php

namespace App\Http\Controllers;

use App\ProductTags;
use App\Tag;
use App\TagTranslation;
use Illuminate\Support\Facades\Request;

class ProductTagController extends HomeController
{
    public function __construct(){
        parent::__construct();
    }

    public function delete($id){
        $element = ProductTags::where('id', $id)->first();
        $element->status = 0;
        if($element->save()) {
            return redirect()->back()->with('success', 'Product tag deleted successful!');
        }
        else{
            return redirect()->back()->with('error', 'Product tag deletion error!');
        }
    }

    public function postSearchTag(){

        $query = Request::get('q');
        $page_limit = Request::get('page_limit');
        $page = Request::get('page');




        /*$tags = Tag::join('tag_translations as t', function ($join) {
            $join->on('tags.id', '=', 't.tag_id')
                /*->where('t.locale', '=', app()->getLocale())*/
                /*->where('t.name', 'LIKE', '%' . Request::get('q') . '%');
            });*/
            /*->groupBy('tag.id')
            ->orderBy('t.name', 'desc');*/
            //->with('translations');


        /*$tags = \App\Tag::where('id', $query);

        $tags = \App\Tag::
        where('id', $query)
            ->orWhere('name', 'LIKE', '%' . $query . '%');*/

        /*$tags = Tag::whereHas('translations', function ($query) {
            $query->where('locale', 'en')->orWhere('locale', 'hu')
                ->where('name', 'LIKE', '%' . Request::get('q') . '%');
        });*/

        $tags = TagTranslation::
        where('tag_id', $query)
            ->orWhere('name', 'LIKE', '%' . $query . '%')
            ->paginate($page_limit);

        return response()->json($tags);

    }

}
