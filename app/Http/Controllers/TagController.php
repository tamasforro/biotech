<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductTags;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Kris\LaravelFormBuilder\FormBuilder;

class TagController extends HomeController
{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $elements = Tag::orderBy('id');

        //dd($elements->first()->translate('en')->name);

        return view('tag.index', ['elements' => $elements]);
    }


    public function create(FormBuilder $formBuilder){

        $form = $formBuilder->create('App\Forms\TagForm', [
            'method' => 'POST',
            //'model' => $element,
            'url' => route('tag.save'),
            'files' => true
        ]);

        return view("tag.form", [ 'form' => $form, 'products' => null ]);
    }

    public function edit($id, FormBuilder $formBuilder){
        $element = Tag::where('id', $id)->first();

        $managed = ProductTags::where('tag_id', $element->id)->get();

        $form = $formBuilder->create('App\Forms\TagForm', [
            'method' => 'POST',
            'model' => $element,
            'url' => route('tag.save', ["id" => $id]),
            'files' => true
        ]);

        return view("tag.form", [ 'form' => $form, 'products' => $managed ]);
    }

    public function delete($id){
        $element = Tag::where('id', $id)->first();
        $element->status = 0;
        if($element->save()) {
            return redirect()->back()->with('success', 'Tag inactivate successful!');
        }
        else{
            return redirect()->back()->with('error', 'Tag inactivation error!');
        }
    }

    public function save($id = null, Request $request, FormBuilder $formBuilder){
        $tag = Tag::where('id', $id)->first();

        if(!$tag){
            $tag = new Tag();
        }

        $form = $formBuilder->create('App\Forms\TagForm', [
            'method' => 'POST',
            'model' => $tag,
            'url' => route('tag.save', ["id" => $id]),
            'files' => true
        ]);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = $form->validate()->getData();


        $tag->status = $request->get('status');
        foreach( Config::get('app.languages') as $key => $lang ) {
            $tag->translateOrNew($key)->name = $data['translate_' . $key . '_name'];
        }





        if($tag->save()) {
            foreach($form->getFields() as $field) {
                if($field->getName() == 'product_collection' && !is_null($field->getRawValue())){
                    $product_ids = explode(';', $field->getRawValue());

                    for($i = 0; $i < count($product_ids); $i++){
                        if(ProductTags::createTag($form->getModel()->id, Product::where('id', $product_ids[$i])->first())['status'] == 'error'){
                            return redirect()->back()->with('error', 'Error during saving a product tag!');
                        }
                    }
                }
            }
            return redirect()->route('tag.index')->with('success', 'Tag saved successful!');
        }
        else{
            return redirect()->back()->with('error', 'Error during saving the tag!');
        }
    }
}
