<?php

namespace App\Http\Controllers;


use App\Product;
use App\ProductDocuments;
use Illuminate\Support\Facades\Storage;

class ProductDocumentController extends HomeController
{
    public function __construct(){
        parent::__construct();
    }

    public function delete($id){
        $element = ProductDocuments::where('id', $id)->first();
        $element->status = 0;

        if($element->save()) {
            return redirect()->back()->with('success', 'Product Document deleted successful!');
        }

        return redirect()->back()->with('error', 'Product Document deletion error!');
    }

    public function download($id){
        $document = ProductDocuments::where('id', $id)->first();
        $product_id = Product::where('id', $document->product_id)->first()->id;

        $path = ProductDocuments::$storage_path . '/'. $product_id . '/' . $document->path;

        $file = Storage::disk('public')->get(
            $path
        );
        $response = response( $file, 200 );
        $response->header("Content-type", Storage::mimeType('public/' . $path));
        $response->header('Content-Disposition', 'attachment; filename="' . $document->path . '"');

        return $response;
    }
}
