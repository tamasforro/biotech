<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductDocuments;
use App\ProductTags;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Kris\LaravelFormBuilder\FormBuilder;

class ProductController extends HomeController
{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $elements = Product::orderBy('id');

        return view('product.index', ['elements' => $elements]);
    }
    public function create(FormBuilder $formBuilder){
        //$element = new Conference();

        $form = $formBuilder->create('App\Forms\ProductForm', [
            'method' => 'POST',
            //'model' => $element,
            'url' => route('product.save'),
            'files' => true
        ]);

        return view("product.form", [
            'form' => $form,
            'documents' => null,
            'tags' => null
        ]);
    }

    public function edit($id, FormBuilder $formBuilder){
        $element = Product::where(['id' => $id])->first();

        $documents = ProductDocuments::where('product_id', $element->id)->get();

        $tags = ProductTags::where('product_id', $element->id)->get();

        $element->publish_start = Carbon::create($element->publish_start)->format('Y-m-d\TH:i:s');
        $element->publish_end = Carbon::create($element->publish_end)->format('Y-m-d\TH:i:s');


        $form = $formBuilder->create('App\Forms\ProductForm', [
            'method' => 'POST',
            'model' => $element,
            'url' => route('product.save', ["id" => $id]),
            'files' => true
        ]);

        //dd($element);

        return view("product.form", [
            'form' => $form,
            'documents' =>  $documents,
            'tags' => $tags
        ]);
    }

    public function delete($id){
        $element = Product::where('id', $id)->first();
        $element->status = 0;
        if($element->save()) {
            return redirect()->back()->with('success', 'Product deleted successful!');
        }
        else{
            return redirect()->back()->with('error', 'Product deletion error!');
        }
    }

    public function save($id = null, Request $request, FormBuilder $formBuilder){
        $element = Product::where('id', $id)->first();

        if(!$element){
            $element = new Product();
        }

        $form = $formBuilder->create('App\Forms\ProductForm', [
            'method' => 'POST',
            'model' => $element,
            'url' => route('product.save', ["id" => $id]),
            'files' => true
        ]);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = $form->validate()->getData();


        $element->status = $request->get('status');
        $element->publish_start = Carbon::createFromFormat('Y-m-d\TH:i:s', $request->get('publish_start'));
        $element->publish_end = Carbon::createFromFormat('Y-m-d\TH:i:s', $request->get('publish_end'));
        foreach( Config::get('app.languages') as $key => $lang ) {
            $element->translateOrNew($key)->name = $data['translate_' . $key . '_name'];
            $element->translateOrNew($key)->description = $data['translate_' . $key . '_description'];
            $element->translateOrNew($key)->price = $data['translate_' . $key . '_price'];
        }

        //dd($request->all());

        if($element->save()) {


            if(!is_null($request->get('tag_collection'))){
                $tag_ids = explode(';', $request->get('tag_collection'));

                for($i = 0; $i < count($tag_ids); $i++){
                    if(ProductTags::createTag($tag_ids[$i], $form->getModel()->id)['status'] == 'error'){
                        return redirect()->back()->with('error', 'Error during saving a product tag!');
                    }
                }
            }


            foreach($form->getFields() as $field) {


                if (Str::contains($field->getName(), 'document')) {
                    $file = $form->getRequest()->file($field->getName());
                    if (!is_null($file)) {
                        //$doc_type = Str::replaceLast('_document', '', $field->getName());
                        if(ProductDocuments::createDocument($file, $form->getModel()->id)['status'] == 'error'){
                            return redirect()->back()->with('error', 'Error during saving a product document!');
                        }
                    }
                }
            }

            return redirect()->route('product.index')->with('success', 'Product saved successful!');
        }
        else {
            return redirect()->back()->with('error', 'Error during saving the product!');
        }
    }
}
