<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /*public function changeLanguage( $lang = '' )
    {

        if( !in_array($lang, array_keys(Config::get('app.languages'))))
            $lang = Config::get('app.locale');

        Session::put('locale', $lang);
        Carbon::setLocale( $lang );

        return redirect()->back();
    }*/
}
