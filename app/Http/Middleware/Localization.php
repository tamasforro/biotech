<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    /*public function handle($request, Closure $next)
    {
        if(Session::has('locale'))
        {
            App::setlocale(Session::get('locale'));
        }
        return $next($request);
    }*/

    public function handle($request, Closure $next)
    {
        $sessionLocale = Session::get('locale', null);
        $current = null;

        if( Auth::check() && !is_null(Auth::user()->profile)){
            $current = Auth::user()->profile->language_id;
        }

        if( !is_null( $sessionLocale ) ) $current = $sessionLocale;

        if( is_null( $current ) ) $current = App::getLocale();

        if ( !in_array( $current, array_keys( Config::get('app.languages') ) ) )
            $current = Config::get('app.fallback_locale');

        App::setLocale( $current );

        return $next( $request );
    }
}
