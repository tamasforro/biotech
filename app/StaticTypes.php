<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticTypes extends Model
{

    public static $status_types = [
        0 => 'static-types.status-types.inactive',
        1 => 'static-types.status-types.active'

    ];

    public static $statuses = [
        0 => 'Inactive',
        1 => 'Active'
    ];

    public static $status_types_badges = [
        0 => 'secondary',
        1 => 'primary',

    ];
}
