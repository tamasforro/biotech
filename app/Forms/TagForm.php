<?php

namespace App\Forms;

use App\Libraries\ListGenerator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;

class TagForm extends Form
{
    public function buildForm()
    {

        if (Request::is('*/edit/*')) {

            foreach (Config::get('app.languages') as $key => $lang) {
                $this
                    ->add('translate.' . $key . '.name', Field::TEXT, [
                        'label' => trans('site.manage.' . $key . '_name'),
                        'value' => $this->getModel()->translate($key)->name,
                        'rules' => 'required'
                    ]);
            }

        }
        else {

            foreach (Config::get('app.languages') as $key => $lang) {
                $this
                    ->add('translate.' . $key . '.name', Field::TEXT, [
                        'label' => trans('site.manage.' . $key . '_name'),
                        'rules' => 'required'
                    ]);
            }
        }


        $this

            ->add('status', Field::CHOICE, [
                'label' => trans('site.fields.status'),
                'choices' => ListGenerator::activeInactiveStatuses()
            ])


            ->add('buttons', 'button-group', [
                'wrapper' => ['class' => 'form-group col-sm-12'],
                'splitted' => false,
                //'size'      => 'lg',
                'buttons' => [
                    "submit" => [
                        "label" => trans('site.buttons.save'),
                        "attr" => [
                            "type" => "submit",
                            "class" => "btn btn-primary"
                        ]
                    ],
                    "cancel" => [
                        "label" => trans('site.buttons.cancel'),
                        "attr" => [
                            //"type"      => "cancel",
                            "class" => "btn btn-outline-secondary",
                            //'href' => route('admin.conference.index')
                            'onclick' => 'window.history.back()'
                        ]
                    ]
                ]

            ]);
    }
}
