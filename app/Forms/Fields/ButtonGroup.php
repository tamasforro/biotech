<?php
namespace App\Forms\Fields;

use Kris\LaravelFormBuilder\Fields\FormField;

class ButtonGroup extends FormField
{

    protected function getTemplate()
    {
        return 'components.custom-fields.button-group';
    }

    public function render(array $options = [], $showLabel = true, $showField = true, $showError = true)
    {
        $options['splitted'] = $this->getOption('splitted', false);
        $options['size'] = $this->getOption('size', 'md');
        $options['buttons'] = $this->getOption('buttons', []);

        return parent::render($options, $showLabel, $showField, $showError);
    }
}
