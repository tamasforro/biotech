<?php

namespace App\Forms;

use App\Libraries\ListGenerator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;

class ProductForm extends Form
{
    public function buildForm()
    {

        if (Request::is('*/edit/*')) {

            foreach( Config::get('app.languages') as $key => $lang ){
                $this
                    ->add('translate.' . $key . '.name', Field::TEXT, [
                        'label' => trans('site.manage.' . $key . '_name'),
                        'value' => $this->getModel()->translate($key)->name,
                        'rules' => 'required'
                    ]);
            }

            $i = 0;
            foreach( Config::get('app.languages') as $key => $lang ){
                $i++;
                $this
                    ->add('translate.' . $key . '.description', Field::TEXTAREA, [
                        'label' => trans('site.manage.' . $key . '_description'),
                        'rules' => 'required|max:5000',
                        'value' => $this->getModel()->translate($key)->description,
                        "attr" => [
                            "id" => "ckeditor" . $i
                        ]
                    ]);
            }

            foreach( Config::get('app.languages') as $key => $lang ){
                $this
                    ->add('translate.' . $key . '.price', Field::TEXT, [
                        'label' => trans('site.manage.' . $key . '_price'),
                        'value' => $this->getModel()->translate($key)->price,
                        'rules' => 'required'
                    ]);
            }
        }
else {

    foreach (Config::get('app.languages') as $key => $lang) {
        $this
            ->add('translate.' . $key . '.name', Field::TEXT, [
                'label' => trans('site.manage.' . $key . '_name'),
                'rules' => 'required'
            ]);
    }

    $i = 0;
    foreach (Config::get('app.languages') as $key => $lang) {
        $i++;
        $this
            ->add('translate.' . $key . '.description', Field::TEXTAREA, [
                'label' => trans('site.manage.' . $key . '_description'),
                'rules' => 'required|max:5000',
                "attr" => [
                    "id" => "ckeditor" . $i
                ]
            ]);
    }

    foreach (Config::get('app.languages') as $key => $lang) {
        $this
            ->add('translate.' . $key . '.price', Field::TEXT, [
                'label' => trans('site.manage.' . $key . '_price'),
                'rules' => 'required'
            ]);
    }

}
        $this

            ->add('status', Field::CHOICE, [
                'label' => trans('site.fields.status'),
                'choices' => ListGenerator::activeInactiveStatuses()
            ])
            ->add('publish_start', FIELD::DATETIME_LOCAL, [
                    'label' => trans('site.fields.publish_start'),
                    'rules' => 'required',
                ]
            )
            ->add('publish_end', FIELD::DATETIME_LOCAL, [
                    'label' => trans('site.fields.publish_end'),
                    'rules' => 'required'
                ]
            )



        ->add('tags', Field::SELECT, [
        'label' => trans('site.fields.new_tags'),
        'attr' => [
            'class' => 'form-control select2 select2-multiple',
            'id' => 'select_tags',
            'multiple' => 'multiple'
        ]
    ])
        ->add('tag_collection', Field::TEXT, [
            'label_show' => false,
            'attr' => [
                'style' => 'display:none',
                'id' => 'tag_collection'
            ]
        ]);



        $this
            ->add('document', Field::FILE, [
                'label' => trans('site.fields.picture_document'),
            ])

            ->add('buttons', 'button-group', [
                'wrapper' => ['class' => 'form-group col-sm-12'],
                'splitted' => false,
                //'size'      => 'lg',
                'buttons' => [
                    "submit" => [
                        "label" => trans('site.buttons.save'),
                        "attr" => [
                            "type" => "submit",
                            "class" => "btn btn-primary"
                        ]
                    ],
                    "cancel" => [
                        "label" => trans('site.buttons.cancel'),
                        "attr" => [
                            //"type"      => "cancel",
                            "class" => "btn btn-outline-secondary",
                            //'href' => route('admin.conference.index')
                            'onclick' => 'window.history.back()'
                        ]
                    ]
                ]
            ]);
    }
}
