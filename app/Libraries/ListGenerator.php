<?php

namespace App\Libraries;

use App\StaticTypes;

class ListGenerator
{
    public static function activeInactiveStatuses(){
        $statuses_array = StaticTypes::$status_types;

        foreach($statuses_array as $key => $value){
            $statuses_array[$key] = trans($value);
        }

        return $statuses_array;
    }
}
