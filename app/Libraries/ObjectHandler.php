<?php

namespace App\Libraries;

use App\Exceptions\CannotGuaranteeUniquenessException;
use App\Models\Conference;
use App\Exceptions\ConferenceNotFoundException;
use App\Models\Order;
use App\Models\Payment;
use App\Models\StaticTypes;
use App\Models\User;
use App\Models\UserTickets;
use App\Notifications\OrderCompletedWithWireTransfer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ObjectHandler
{
    protected static $currentConferenceObject;

    public static function getCurrentConferenceObject() : Conference
    {
        if (!self::$currentConferenceObject) {

            $element = Conference::where('id', app('request')->input('id'))->first();
            if (!$element) {
                throw new ConferenceNotFoundException();
            }

            self::$currentConferenceObject = $element;
        }

        return self::$currentConferenceObject;
    }

    public static function getTickets() {
      return self::getCurrentConferenceObject()->tickets();
    }

    public static function getCartId(){
        return Auth::user()->id . "-" . DomainHandler::getCurrentConference()->id;
    }

    /*public static function getDefaultInvitationText(){
      return '
                <p>Kedves [[name]]!</p>
                <p>Az uni-jegy.hu oldalon jogost vagy 1 db [[ticket_name]] jegy megv&aacute;s&aacute;rl&aacute;sra.</p>
                <p>Jogosults&aacute;godat az e-mail c&iacute;med alapj&aacute;n ellenőrizz&uuml;k.</p>
                <p>&Uuml;dv&ouml;zlettel,</p>
                <p>uni-jegy.hu</p>
              ';
    }*/

    public static function replaceParametersWithValue($content){
        $conference = DomainHandler::getCurrentConference();

        $date = '';

        if(\Carbon\Carbon::create($conference->start_date)->shortMonthName == \Carbon\Carbon::create($conference->end_date)->shortMonthName) {

            $date = \Carbon\Carbon::create($conference->start_date)->shortMonthName . ' ' .
                \Carbon\Carbon::create($conference->start_date)->day . '-' .
                \Carbon\Carbon::create($conference->end_date)->day . ' ' .
                \Carbon\Carbon::create($conference->end_date)->year;

        }
        else {

            $date = \Carbon\Carbon::create($conference->start_date)->shortMonthName . ' ' .
                \Carbon\Carbon::create($conference->start_date)->day . '-' .
                \Carbon\Carbon::create($conference->end_date)->shortMonthName . ' ' .
                \Carbon\Carbon::create($conference->end_date)->day . ' ' .
                \Carbon\Carbon::create($conference->end_date)->year;

        }

        $result = $content;
        $result = preg_replace('/@{conference_name}/', $conference->name, $result);
        $result = preg_replace('/@{conference_contact}/', $conference->support_contact, $result);
        $result = preg_replace('/@{conference_home}/', route('home'), $result);
        $result = preg_replace('/@{conference_location}/', $conference->location()->first()->toCompleteLocation(), $result);
        $result = preg_replace('/@{conference_date}/', $date, $result);
        $result = preg_replace('/<a /', '<a style="text-decoration: none; color:#04cd8f; "' , $result);

        return $result;
    }


    public static function getOptionKey($getKey, $defaultValue = null)
    {
        $result = $defaultValue;
        $options = [
            'barion_is_test' => true,
            'barion_test_pos_key' => 'e3eb0a5942364e948d283bd9047bb01b',
            'barion_test_email_addres' => 'tomhot_12@hotmail.com',
            'barion_test_shop_name' => Conference::where('id', Auth::user()->conference_id)->first()->name,
            'conference_short_name' => Conference::where('id', Auth::user()->conference_id)->first()->name,
            'barion_api_version' => 2,

            'barion_production_pos_key' => '9c165cfc-cbd1-452f-8307-21a3a9cee664',
            'barion_production_email_address' => '',
            'barion_production_shop_name' => Conference::where('id', Auth::user()->conference_id)->first()->name,
        ];

        foreach($options as $key => $value){
            if($key == $getKey){
                $result = $value;
                break;
            }
        }

        return $result;
    }

    public static function guaranteeUniqueness($iteration, $max_iteration, $length){
        if($iteration < $max_iteration) {
            $uuid = Str::random($length);
            if (User::where('uuid', $uuid)->count() > 0) {
                ObjectHandler::guaranteeUniqueness($iteration + 1, $max_iteration, $length);
            } else {
                return $uuid;
            }
        }
        else{
            if($length < 36){
                $iteration = -1;
                $length++;
                ObjectHandler::guaranteeUniqueness($iteration + 1, $max_iteration, $length);
            }
            else{
                throw new CannotGuaranteeUniquenessException();
            }

        }
    }

    public static function createLinkWithSubDomain($link, $conference_id){
        $domain = Conference::where('id', $conference_id)->first()->domain;
        $input_prefix = explode('://', $link)[0];
        $input_url = explode('://', $link)[1];
        $input_domain = explode('/', $input_url, 2)[0];
        if($input_domain === $domain) {
            return $link;
        }
        else {
            $input_url = str_replace($input_domain, $domain, $input_url);
            $link = $input_prefix . '://' . $input_url;
            return $link;
        }
    }

    public static function writeLogLine($output, $with_ts = true){
        if($with_ts) {
            \Illuminate\Support\Facades\Log::info(\Illuminate\Support\Carbon::now(). ' - ' . $output);
        }
        else{
            \Illuminate\Support\Facades\Log::info($output);
        }
    }

    public static function setObjectsStatus($ids, $model, $action)
    {
        $elements = null;
        if ($model == 'user') {
            $elements = User::whereIn('id', $ids)->get();
        }

        if ($model == 'user-ticket') {
            $elements = UserTickets::whereIn('id', $ids)->get();
        }

        if ($model == 'order') {
            $elements = Order::whereIn('id', $ids)->get();
        }

        if ($model == 'payment') {
            $elements = Payment::whereIn('id', $ids)->get();
        }

        if ($elements->count() > 0) {
            switch($action){
                case 'inactive':
                    $status = 0;
                    $status_text = ListGenerator::activeInactiveStatuses()[$status];
                    $badge = StaticTypes::$status_types_badges[$status];
                    break;
                case 'active':
                    $status = 1;
                    $status_text = ListGenerator::activeInactiveStatuses()[$status];
                    $badge = StaticTypes::$status_types_badges[$status];
                    break;
                case 'valid':
                    $status = 1;
                    $status_text = ListGenerator::ticketStatuses()[$status];
                    $badge = StaticTypes::$ticket_statuses_badges[$status];
                    break;
                case 'pending':
                    $status = 3;
                    $status_text = ListGenerator::ticketStatuses()[$status];
                    $badge = StaticTypes::$ticket_statuses_badges[$status];
                    break;
                case 'withdrawn':
                    $status = 2;
                    $status_text = ListGenerator::ticketStatuses()[$status];
                    $badge = StaticTypes::$ticket_statuses_badges[$status];
                    break;
                case 'deleted':
                    $status = 0;
                    $status_text = ListGenerator::ticketStatuses()[$status];
                    $badge = StaticTypes::$ticket_statuses_badges[$status];
                    break;
                case 'paid':
                    $status = 2;
                    $status_text = ListGenerator::orderStatuses()[$status];
                    $badge = StaticTypes::$order_status_badges[$status];
                    break;
                case 'timeout':
                    if($model == 'order') {
                        $status = 3;
                        $status_text = ListGenerator::orderStatuses()[$status];
                        $badge = StaticTypes::$order_status_badges[$status];
                    }
                    else{
                        $status = 8;
                        $status_text = ListGenerator::paymentStatuses()[$status];
                        $badge = StaticTypes::$payment_status_badges[$status];
                    }
                    break;
                case 'started':
                    $status = 1;
                    $status_text = ListGenerator::paymentStatuses()[$status];
                    $badge = StaticTypes::$payment_status_badges[$status];
                    break;

                case 'in_progress':
                    $status = 2;
                    $status_text = ListGenerator::paymentStatuses()[$status];
                    $badge = StaticTypes::$payment_status_badges[$status];
                    break;

                case 'waiting':
                    $status = 3;
                    $status_text = ListGenerator::paymentStatuses()[$status];
                    $badge = StaticTypes::$payment_status_badges[$status];
                    break;

                case 'terminated_by_user':
                    $status = 4;
                    $status_text = ListGenerator::paymentStatuses()[$status];
                    $badge = StaticTypes::$payment_status_badges[$status];
                    break;

                case 'success':
                    $status = 5;
                    $status_text = ListGenerator::paymentStatuses()[$status];
                    $badge = StaticTypes::$payment_status_badges[$status];
                    break;

                case 'failed':
                    $status = 6;
                    $status_text = ListGenerator::paymentStatuses()[$status];
                    $badge = StaticTypes::$payment_status_badges[$status];
                    break;

                case 'partial_success':
                    $status = 7;
                    $status_text = ListGenerator::paymentStatuses()[$status];
                    $badge = StaticTypes::$payment_status_badges[$status];
                    break;

                default:
                    return response()->json([
                        'type' => 'error',
                        'title' => trans('shared.messages.models-action.error.title'),
                        'text' => trans('shared.messages.models-action.error.text'),
                        'footer' => trans('shared.messages.models-action.error.footer')
                    ]);
            }
            foreach ($elements as $element) {
                $element->status = $status;
                $element->save();
                if($model == 'payment' && $action == 'success'){
                    $order = Order::where('id', $element->order_id)->first();
                    $order->status = 2;

                    $user_tickets = UserTickets::where('order_id', $element->order_id)->get();

                    if ($user_tickets->count() > 0) {
                        foreach ($user_tickets as $user_ticket) {
                            $user_ticket->status = 1;

                            $user_ticket->createVoucher();
                            $user_ticket->save();
                        }
                    }
                    $order->save();
                    $order->user()->first()->notify(new OrderCompletedWithWireTransfer($order, $element));
                }
            }
        }

        return response()->json([
            'type' => 'success',
            'title' => '',
            'text' => '',
            'footer' => '',
            'status' => $status_text,
            'badge' => 'badge-' . $badge
        ]);
    }
}
