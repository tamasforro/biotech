<?php

namespace App\Libraries;


use App\Exceptions\CannotGuaranteeUniquenessException;
use App\Exceptions\DomainNotFoundException;
use App\Models\Conference;
use App\Exceptions\ConferenceNotFoundException;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class DomainHandler
{
    protected static $currentConference;
    protected static $currentSubDomain;

    public static function getCurrentConference()
    {
        if (!is_null(self::$currentConference)) {
            return self::$currentConference;
        } else {
            self::$currentConference = Conference::where('domain', self::getCurrentSubDomain())->first();
        }
        return self::$currentConference;
    }

    public static function getCurrentTemplateCode()
    {
        self::getCurrentConference();
        if(!is_null(self::getCurrentConference()) && self::getCurrentConference()->template()->count() > 0){
            return 'templates.' . self::$currentConference->template->code;
        }
        return 'templates.default';
    }

    public static function getCurrentTemplatePath()
    {
        self::getCurrentConference();
        if(!is_null(self::getCurrentConference()) && self::getCurrentConference()->template()->count() > 0){
            return 'templates/' . self::$currentConference->template->code;
        }
        return 'templates/default';
    }

    public static function getCurrentSubDomain()
    {
        if (!is_null((self::$currentSubDomain))) {
            return self::$currentSubDomain;
        } else {
            self::$currentSubDomain = parse_url(URL::to('/'), PHP_URL_HOST);
        }
        return self::$currentSubDomain;
    }
}
