<tr>
    <td>
        {{ $tag->tag()->first()->id }}
    </td>
    <td>
        {{ $tag->tag()->first()->translate(app()->getLocale())->name }}
    </td>
    <td>
        {!! $tag->getStatusForTable() !!}
    </td>
    <td>
        {{ $tag->created_at }}
    </td>
    <td>
        <a href="{{ route('tag.delete', ["id" => $tag->id]) }}" class="btn-sm btn-danger">{{ trans('site.table.delete') }}</a>
    </td>
</tr>
