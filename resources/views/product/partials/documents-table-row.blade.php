<tr>
    <td>
        {{ $document->id }}
    </td>
    <td>
        {{ $document->path }}
    </td>
    <td>
        {!! $document->getStatusForTable() !!}
    </td>

    <td>
        {{ $document->created_at }}
    </td>
    <td>
        <a href="{{ route('document.download', ["id" => $document->id]) }}" class="btn-sm btn-success">{{ trans('site.table.download') }}</a>
        <a href="{{ route('document.delete', ["id" => $document->id]) }}" class="btn-sm btn-danger">{{ trans('site.table.delete') }}</a>
    </td>
</tr>
