@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="animated fadeIn">


            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#product" role="tab" aria-controls="product">{{ trans('site.models.product') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tags" role="tab" aria-controls="tags">{{ trans('site.actions.assigned_tags') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#documents" role="tab" aria-controls="documents">{{ trans('site.actions.related_documents') }}</a>
                </li>

            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="product" role="tabpanel">
                    <div class="card">
                        <div class="card-header">
                            {{ trans('site.models.product') }}
                        </div>
                        <div class="card-body">
                            {!! form($form) !!}
                        </div>
                    </div>

                </div>




                <div class="tab-pane" id="tags" role="tabpanel">

                    @if(!is_null($tags))
                        <div class="card">
                            <div class="card-header">
                                {{ trans('site.actions.assigned_tags') }}
                            </div>
                            <div class="card-body">
                                @if($tags->count())
                                    <table class="table table-responsive-sm table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ trans('site.table.name') }}</th>
                                            <th>{{ trans('site.table.status') }}</th>
                                            <th>{{ trans('site.table.created_at') }}</th>
                                            <th>{{ trans('site.table.actions') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($tags as $tag)
                                            @include('product.partials.tags-table-row')
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <h6>{{ trans('site.actions.no_tag_found') }}.</h6>
                                @endif
                            </div>
                        </div>
                    @endif

                </div>




                <div class="tab-pane" id="documents" role="tabpanel">
                    @if(!is_null($documents))
                        <div class="card">
                            <div class="card-header">
                                {{ trans('site.actions.related_documents') }}
                            </div>
                            <div class="card-body">
                                @if($documents->count())
                                    <table class="table table-responsive-sm table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ trans('site.table.path') }}</th>
                                            <th>{{ trans('site.table.status') }}</th>
                                            <th>{{ trans('site.table.created_at') }}</th>
                                            <th>{{ trans('site.table.actions') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($documents as $document)
                                            @include('product.partials.documents-table-row')
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <h6>{{ trans('site.actions.no_document_found') }}.</h6>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>








            </div>

        </div>
    </div>


@endsection

