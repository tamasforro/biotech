
<tr>
    <td>
        {{ $element->id }}
    </td>
    <td>
        {{ $element->translate(app()->getLocale())->name }}
    </td>
    <td>
        {!! $element->getStatusForTable() !!}
    </td>
    <td>
        {{ $element->created_at }}
    </td>
    <td>
        <a href="{{ route('tag.edit', ["id" => $element->id]) }}" class="btn-sm btn-warning">{{ trans('site.table.edit') }}</a>
        <a href="{{ route('tag.delete', ["id" => $element->id]) }}" class="btn-sm btn-danger">{{ trans('site.table.delete') }}</a>
    </td>
</tr>
