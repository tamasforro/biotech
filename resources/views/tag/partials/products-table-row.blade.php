<tr>
    <td>
        {{ $product->id }}
    </td>
    <td>
        {{ $product->name }}
    </td>
    <td>
        {!! $product->getStatusForTable() !!}
    </td>
    <td>
        {{ $product->publish_start }}
    </td>
    <td>
        {{ $product->publish_end }}
    </td>
    <td>
        {{ $product->created_at }}
    </td>
    <td>
        <a href="{{ route('product.edit', ["id" => $product->id]) }}" class="btn btn-warning">{{ trans('site.table.edit') }}</a>
        <a href="{{ route('product.delete', ["id" => $product->id]) }}" class="btn btn-danger">{{ trans('site.table.delete') }}</a>
    </td>
</tr>

