@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="animated fadeIn">

            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tag" role="tab" aria-controls="tag">{{ trans('site.models.tag') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#products" role="tab" aria-controls="products">{{ trans('site.models.product') }}</a>
                </li>
            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="tag" role="tabpanel">

                    <div class="card">
                        <div class="card-header">
                            {{ trans('site.models.tag') }}
                        </div>
                        <div class="card-body">
                            {!! form($form) !!}
                        </div>
                    </div>


                </div>
                <div class="tab-pane" id="products" role="tabpanel">

                    @if(!is_null($products))
                        <div class="card">
                            <div class="card-header">
                                {{ trans('site.actions.connected_products') }}
                            </div>
                            <div class="card-body">
                                @if($products->count())
                                    <table class="table table-responsive-sm table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Publish start</th>
                                            <th>Publish end</th>
                                            <th>Created at</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($products as $product)
                                            @include('tag.partials.products-table-row')
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <h6>{{ trans('site.actions.no_connected_products') }}</h6>
                                @endif
                            </div>
                        </div>
                    @endif

                </div>

            </div>
        </div>


@endsection

