@extends('layouts.app')


@section('content')

    @include('components.messages')

    <div class="container">
        <div class="animated fadeIn">

            <div class="card">
                <div class="card-header">
                    {{ trans('site.models.tag') }}
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <a href="{{ route('home') }}" class="btn btn-info"><i class="fa fa-plus"></i>
                            {{ trans('site.actions.back') }}</a>

                        <a href="{{ route('tag.create') }}" class="btn btn-info"><i class="fa fa-plus"></i>
                            {{ trans('site.actions.create_new_tag') }}</a>
                    </div>

                    @if($elements->count())
                        <table class="table table-responsive-sm table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ trans('site.table.name') }}</th>
                                <th>{{ trans('site.table.status') }}</th>
                                <th>{{ trans('site.table.created_at') }}</th>
                                <th>{{ trans('site.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($elements->get() as $element)

                                @include('tag.partials.tags-table-row')
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <h6>{{ trans('site.actions.no_tag_found') }}.</h6>
                    @endif
                </div>
            </div>

        </div>
    </div>


@endsection

