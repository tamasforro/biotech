@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ trans('site.home.welcome') }}
                        <br>
                        <br>
                        <button class="btn btn-outline-info" onclick="window.location='{{ route('product.index') }}'">{{ trans('site.home.buttons.show_products') }}</button>
                        <button class="btn btn-outline-info" onclick="window.location='{{ route('tag.index') }}'">{{ trans('site.home.buttons.show_tags') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
