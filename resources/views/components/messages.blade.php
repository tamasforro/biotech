<div class="container session-message">
@if ($message = Session::get('success'))
<div class="m-alert m-alert--outline alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
	<span>
		<strong>Success:</strong> {!! $message !!}
	</span>
</div>
{{ Session::forget('success') }}

@elseif ($message = Session::get('error'))
<div class="m-alert m-alert--outline alert alert-danger alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
	<span>
		<strong>Error:</strong> 
		@if( is_string($message) )
			{!! $message !!}
			{{ Session::forget('error') }}
		@elseif( is_object( $message ) )
			@foreach ($message->all() as $key => $m)
					<p>{{ $m }}</p>
			@endforeach
		@endif
	</span>
</div>

@elseif ($message = Session::get('warning'))
<div class="alert alert-warning">
	<div class="container">
	<strong>Warning:</strong> {!! $message !!}
	</div>
</div>
{{ Session::forget('warning') }}

@elseif ($message = Session::get('info'))
<div class="alert alert-info">
	<div class="container">
	<strong>FYI:</strong> {!! $message !!}
	</div>
</div>
{{ Session::forget('info') }}

@elseif( isset( $errors ) && count( $errors->all() ) > 0 )
<div class="alert alert-danger">
	<div class="container">
    @foreach ($errors->all() as $key => $message)
        <p>{{ $message }}</p>
    @endforeach
  </div>
</div>
@endif
</div>
