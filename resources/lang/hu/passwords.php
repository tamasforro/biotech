<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A jelszónak legalább 8 katakter hosszúnak kell lennie, és egyeznie kell a megerősítésben megadottal.',
    'reset' => 'A jelszava helyreállításra került!',
    'sent' => 'A jelszó helyreállítási email kiküldésre került!',
    'token' => 'A jelszó helyreállítási token érvénytelen.',
    'user' => "Nem található felhasználó a megadott email címmel!",

];
