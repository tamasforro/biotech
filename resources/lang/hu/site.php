<?php

return [

    'home' => [
        'welcome' => 'Üdvözlünk a BioTech Termékkezelőben!',
        'buttons' => [
            'show_products' => 'Terméklista',
            'show_tags' => 'Címkelista'
        ]
    ],

    'models' => [
        'tag' => 'Címke',
        'product' => 'Termék',
        'document' => 'Dokumentum'
    ],

    'actions' => [
        'create_new_tag' => 'Új címke létrehozása',
        'create_new_product' => 'Új termék létrehozása',
        'no_tag_found' => 'Nem található címke',
        'no_document_found' => 'Nem található dokumentum',
        'no_product_found' => 'Nem található termék',
        'connected_products' => 'Hozzákapcsolt termékek',
        'no_connected_products' => 'Nincsenek hozzákapcsolt termékek',
        'back' => 'Vissza',
        'assigned_tags' => 'Hozzárendelt címkék',
        'related_documents' => 'Kapcsolódó dokumentumok'
    ],

    'manage' => [
        'en_name' => 'Angol név',
        'hu_name' => 'Magyar név',
        'en_description' => 'Angol leírás',
        'hu_description' => 'Magyar leírás',
        'en_price' => 'Angol ár',
        'hu_price' => 'Magyar ár'
    ],
    'fields' => [
        'status' => 'Státusz',
        'publish_start' => 'Publikálás kezdete',
        'publish_end' => 'Publikálás vége',
        'tags' => 'Címkék',
        'picture_document' => 'Kép dokumentum',
        'new_tags' => 'Új címke hozzárendelése'
    ],
    'buttons' => [
        'save' => 'Mentés',
        'cancel' => 'Mégsem'
    ],

    'table' => [
        'edit' => 'Szerkesztés',
        'delete' => 'Törlés',
        'name' => 'Név',
        'status' => 'Státusz',
        'created_at' => 'Létrehozva',
        'actions' => 'Tevékenység',
        'publish_start' => 'Publikálás kezdete',
        'publish_end' => 'Publikálás vége',
        'price' => 'Ár',
        'description' => 'Leírás',
        'path' => 'Útvonal',
        'download' => 'Letöltés'
    ]
];
