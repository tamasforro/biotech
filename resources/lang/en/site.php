<?php

return [

    'home' => [
        'welcome' => 'Welcome to BioTech Product Management!',
        'buttons' => [
            'show_products' => 'Show Products',
            'show_tags' => 'Show Tag'
        ]
    ],

    'models' => [
        'tag' => 'Tag',
        'product' => 'Product',
        'document' => 'Document'
    ],

    'actions' => [
        'create_new_tag' => 'Create new tag',
        'create_new_product' => 'Create new product',
        'no_tag_found' => 'No tag found',
        'no_document_found' => 'No document found',
        'no_product_found' => 'No product found',
        'connected_products' => 'Connected products',
        'no_connected_products' => 'Connected products found',
        'back' => 'Back',
        'assigned_tags' => 'Assigned tags',
        'related_documents' => 'Related documents'
    ],

    'manage' => [
        'en_name' => 'English name',
        'hu_name' => 'Hungarian name',
        'en_description' => 'English description',
        'hu_description' => 'Hungarian description',
        'en_price' => 'English price',
        'hu_price' => 'Hungarian price'
    ],
    'fields' => [
        'status' => 'Status',
        'publish_start' => 'Publish start',
        'publish_end' => 'Publish end',
        'tags' => 'Tags',
        'picture_document' => 'Picture document',
        'new_tags' => 'Add new tag'
    ],
    'buttons' => [
        'save' => 'Save',
        'cancel' => 'Cancel'
    ],

    'table' => [
        'edit' => 'Edit',
        'delete' => 'Delete',
        'name' => 'Name',
        'status' => 'Status',
        'created_at' => 'Created at',
        'actions' => 'Actions',
        'publish_start' => 'Publish start',
        'publish_end' => 'Publish end',
        'price' => 'Price',
        'description' => 'Description',
        'path' => 'Path',
        'download' => 'Download'
    ]
];
