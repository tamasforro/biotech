<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', function ($locale) {
    \Illuminate\Support\Facades\Session::put('locale', $locale);
    return redirect()->back();
})->name('locale');

Route::get('/', function () {
    return view('welcome');
});



//Route::get('lang/{lang}', ['as' => 'site.changeLanguage', 'uses' => 'HomeController@changeLanguage']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::prefix('tag')->name('tag.')->group(function () {
    Route::get('index', 'TagController@index')->name('index');
    Route::get('edit/{id}', 'TagController@edit')->name('edit');
    Route::get('create', 'TagController@create')->name('create');
    Route::get('delete/{id}', 'TagController@delete')->name('delete');
    Route::post('save/{id?}', 'TagController@save')->name('save');
});

Route::prefix('product')->name('product.')->group(function () {
    Route::get('index', 'ProductController@index')->name('index');
    Route::get('edit/{id}', 'ProductController@edit')->name('edit');
    Route::get('create', 'ProductController@create')->name('create');
    Route::get('delete/{id}', 'ProductController@delete')->name('delete');
    Route::post('save/{id?}', 'ProductController@save')->name('save');
});

Route::prefix('product/document')->name('document.')->group(function () {
    Route::get('download/{id}', 'ProductDocumentController@download')->name('download');
    Route::get('delete/{id}', 'ProductDocumentController@delete')->name('delete');
});


Route::post('tags/search', 'ProductTagController@postSearchTag')->name('tags.search.select2');
